#
# ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Programs
export EDITOR=vim

alias startx='ssh-agent startx'

# Make ls use colors
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

# Make xterm use transparency
[ -n "$XTERM_VERSION" ] && transset-df -a >/dev/null

# Add virtualenvwrapper
export WORKON_HOME=~/.virtualenvs
source /usr/bin/virtualenvwrapper.sh
