all:
	make bash
	make xmonad
	make git
	make vim
	make indent
	make xorg
	make tmux

bash:
	cp bashrc ~/.bashrc

xmonad:
	cp xmonad.hs ~/.xmonad/xmonad.hs
	cp xmobarrc ~/.xmobarrc
	xmonad --recompile

tmux:
	cp tmux.conf ~/.tmux.conf
	tmux source-file ~/.tmux.conf

git:
	cp gitconfig ~/.gitconfig

vim:
	cp vimrc ~/.vimrc

indent:
	cp indent.pro ~/.indent.pro

xorg:
	cp xorg/xinitrc ~/.xinitrc
	cp xorg/xprofile ~/.xprofile
	cp xorg/Xdefaults ~/.Xdefaults
	cp xorg/Xresources ~/.Xresources
	cp xorg/xmodmaprc ~/.xmodmaprc
